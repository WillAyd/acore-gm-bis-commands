.go taxi 128

.levelup holypriest 69
.learn all my spells
.setskill 54 350 -- maces
.setskill 136 350 -- staves
.setskill 173 350 -- daggers
.setskill 762 300  -- riding
.maxskill
.send items holypriest "enjoy the mounts" "enjoy" 32458:1 19872:1
.send items holypriest "enjoy the bags" "enjoy" 38082:4
.send items holypriest "here is your TBC BIS" "enjoy" 34339:1 33281:1 34202:1 32524:1 34233:1 34435:1 34342:1 34527:1 34170:1 34562:1 34363:1 32528:1
.send items holypriest "here is your TBC BIS" "enjoy" 19288:1 28823:1 34335:1 34206:1 34348:1
.send money holypriest "here's some gold to work with" "enjoy" 1000000000

.levelup warlock 69
.learn all my spells
.setskill 54 350 -- maces
.setskill 136 350 -- staves
.setskill 173 350 -- daggers
.setskill 755 350 -- JC for BIS neck
.setskill 762 300  -- riding
.maxskill
.send items warlock "enjoy the mounts" "enjoy" 32458:1 19872:1
.send items warlock "enjoy the bags" "enjoy" 38082:4
.send items warlock "here is your TBC BIS" "enjoy" 34339:1 34359:1 31054:1 34242:1 34233:1 34436:1 34344:1 34541:1 34181:1 34564:1 34362:1 34230:1
.send items warlock "here is your TBC BIS" "enjoy" 34429:1 32483:1 34336:1 34179:1 34347:1
.send money warlock "here's some gold to work with" "enjoy" 1000000000

.levelup bmhunter 69
.learn all my spells
.setskill 45 350 -- bows
.setskill 46 350 -- guns
.setskill 55 350 -- 2H swords
.setskill 118 1 -- dual wield
.setskill 172 350 -- 2H axes
.setskill 226 350 -- crossbows
.setskill 229 350 -- polearms
.setskill 473 350 -- fist weapons
.setskill 413 1 -- mail
.setskill 762 300  -- riding
.maxskill
.send items bmhunter "enjoy the mounts" "enjoy" 32458:1 19872:1
.send items bmhunter "enjoy the bags" "enjoy" 38082:3
.send items bmhunter "enjoy the quiver" "enjoy" 34105:1
.send items bmhunter "here is your TBC BIS" "enjoy" 34333:1 34177:1 31006:1 34241:1 34397:1 34443:1 34370:1 34549:1 34188:1 34570:1 34189:1 34361:1
.send items bmhunter "here is your TBC BIS" "enjoy" 33831:1 34427:1 34331:1 34329:1 34334:1
.send money bmhunter "here's some gold to work with" "enjoy" 1000000000

.levelup restoshaman 69
.learn all my spells
.setskill 165 350 -- LW for BIS gear
.setskill 413 1 -- mail
.setskill 762 300  -- riding
.maxskill
.send items restoshaman "enjoy the mounts" "enjoy" 32458:1 19872:1
.send items restoshaman "enjoy the bags" "enjoy" 38082:4
.send items restoshaman "here is your TBC BIS" "enjoy" 34402:1 33281:1 31022:1 32524:1 34375:1 34438:1 32328:1 34543:1 34383:1 34565:1 32528:2 
.send items restoshaman "here is your TBC BIS" "enjoy" 32496:1 34430:1 34335:1 34206:1 28523:1
.send money restoshaman "here's some gold to work with" "enjoy" 1000000000

.levelup restodruid 69
.learn all my spells
.setskill 762 300  -- riding
.maxskill
.send items restodruid "enjoy the mounts" "enjoy" 32458:1 19872:1
.send items restodruid "enjoy the bags" "enjoy" 38082:4
.send items restodruid "here is your TBC BIS" "enjoy" 34245:1 33281:1 34209:1 32337:1 34212:1 34445:1 34372:1 34554:1 34384:1 34571:1 29309:1 34166:1
.send items restodruid "here is your TBC BIS" "enjoy" 29376:1 38288:1 34335:1 34206:1 27886:1
.send money restodruid "here's some gold to work with" "enjoy" 1000000000

.levelup enhshaman 69
.learn all my spells
.setskill 165 350 -- LW for BIS gear
.setskill 118 1 -- dual wield
.setskill 473 350 -- fist weapons
.setskill 413 1 -- mail
.setskill 762 300  -- riding
.maxskill
.send items enhshaman "enjoy the mounts" "enjoy" 32458:1 19872:1
.send items enhshaman "enjoy the bags" "enjoy" 38082:4
.send items enhshaman "here is your TBC BIS" "enjoy" 34244:1 34177:1 34392:1 34241:1 34397:1 34439:1 34343:1 34545:1 34188:1 34567:1 34189:1 32497:1
.send items enhshaman "here is your TBC BIS" "enjoy" 34427:1 34472:1 34331:1 34346:1 33507:1
.send money enhshaman "here's some gold to work with" "enjoy" 1000000000

.levelup feral_tank 69
.learn all my spells
.send items feral_tank "here is your TBC BIS" "enjoy" 34404:1 34178:1 34392:1 34190:1 34211:1 34444:1 34408:1 35156:1 34385:1 34573:1 34213:1
.send items feral_tank "here is your TBC BIS" "enjoy" 34361:1 32501:1 32658:1 30883:1 32387:1
.send items feral_tank "here are your gems" "enjoy" 32212:1 25896:1 32200:11

# pre-raid wotlk bis
.send items feral_tank "here is your wrath pre-raid bis" "enjoy" 37636:1 37861:1 37139:1 37840:1 37165:1 37409:1 37374:1 37183:1 37194:1
.send items feral_tank "here is your wratch pre-raid bis" "enjoy" 37666:1 37624:1 37642:1 37166:1 37390:1 37883:1 
